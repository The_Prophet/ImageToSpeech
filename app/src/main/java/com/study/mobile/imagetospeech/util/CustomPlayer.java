package com.study.mobile.imagetospeech.util;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class CustomPlayer extends AsyncTask<String, String, String> {

    private static final String TAG = CustomPlayer.class.getName();
    private static final String SERVER_URL = "http://172.16.176.77:8090";

    private MediaPlayer mediaPlayer;

    @Override
    protected String doInBackground(String... data) {

        String text = data[0];
        String fileName = getMp3FileName(text);
        Log.d(TAG, "File name: " + fileName);

        String musicURL = SERVER_URL + "/audio/" + fileName;
        Log.d(TAG, "Music url: " + musicURL);

        try {
            playAudio(musicURL);
        } catch (Exception e) {
            Log.e(TAG, "Error while playing music file");
        }

        return "";
    }

    private void playAudio(String url) throws Exception
    {
        killMediaPlayer();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(url);
        mediaPlayer.prepare();
        mediaPlayer.start();
    }

    private void killMediaPlayer() {
        if(mediaPlayer!=null) {
            try {
                mediaPlayer.release();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getMp3FileName(String text) {
        String result = "";

        HttpClient httpclient = new DefaultHttpClient();

        List<NameValuePair> params = new LinkedList<NameValuePair>();
        params.add(new BasicNameValuePair("text", text));

        Log.d(TAG, "Setting params for request");
        String urlWithParams = SERVER_URL + "?";
        urlWithParams += URLEncodedUtils.format(params, "utf-8");

        HttpGet httppost = new HttpGet(urlWithParams);

        try {
            String toString = httppost.toString();
            String toURI = httppost.getURI().toString();

            HttpResponse response = httpclient.execute(httppost);

            if (response != null) {
                result = EntityUtils.toString(response.getEntity());
            }

            Log.d(TAG, "response " + response.getEntity());
            Log.d(TAG, "result: " + result);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}