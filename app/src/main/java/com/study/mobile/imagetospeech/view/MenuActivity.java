package com.study.mobile.imagetospeech.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.study.mobile.imagetospeech.R;

public class MenuActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onBackButtonClick(View view) {
        finish();
    }

    public void onAboutButtonClick(View view) {
        startActivity(new Intent(MenuActivity.this, AboutActivity.class));
    }
}
