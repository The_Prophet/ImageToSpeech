package com.study.mobile.imagetospeech.view;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.study.mobile.imagetospeech.R;
import com.study.mobile.imagetospeech.util.RequestPermissionsTool;
import com.study.mobile.imagetospeech.util.RequestPermissionsToolImpl;
import com.study.mobile.imagetospeech.util.TesseractWrapper;

import java.io.File;

import static com.study.mobile.imagetospeech.util.TesseractWrapper.prepareDirectory;

public class CameraActivity extends Activity implements ActivityCompat.OnRequestPermissionsResultCallback  {

    private static final String TAG = CameraActivity.class.getSimpleName();
    private static final int PHOTO_REQUEST_CODE = 1;

    private static final String DATA_PATH = Environment.getExternalStorageDirectory().toString()
            + "/TesseractSample/";

    private EditText editText;
    private Uri outputFileUri;

    private RequestPermissionsTool requestTool; //for API >=23 only

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        Button captureImg = (Button) findViewById(R.id.action_btn);
        if (captureImg != null) {
            captureImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startCameraActivity();
                }
            });
        }
        editText = (EditText) findViewById(R.id.editText);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String text = extras.getString("text");
            if (text != null && !text.isEmpty()) {
                editText.setText(text);
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions();
        }
    }

    private void startCameraActivity() {
        try {
            String imagesDir = DATA_PATH + "imgs";
            prepareDirectory(imagesDir);

            String imgPath = imagesDir + "/ocr.jpg";

            outputFileUri = Uri.fromFile(new File(imgPath));

            final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, PHOTO_REQUEST_CODE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {
        //making photo
        if (requestCode == PHOTO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {

            TesseractWrapper tess = new TesseractWrapper(this);
            String result = tess.doOCR(outputFileUri);
            editText.setText(result);
        } else {
            Toast.makeText(this, "ERROR: Image was not obtained.", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestPermissions() {
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        requestTool = new RequestPermissionsToolImpl();
        requestTool.requestPermissions(this, permissions);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        boolean grantedAllPermissions = true;
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                grantedAllPermissions = false;
            }
        }

        if (grantResults.length != permissions.length || (!grantedAllPermissions)) {
            requestTool.onPermissionDenied();
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void onBackButtonCameraClick(View view) {
        Intent data = new Intent();

        EditText editText = (EditText) findViewById(R.id.editText);
        String text = editText.getText().toString().trim();
        data.putExtra("textToSpeech", text);
        setResult(RESULT_OK, data);

        finish();
    }
}