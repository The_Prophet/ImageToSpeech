package com.study.mobile.imagetospeech.view;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.study.mobile.imagetospeech.R;
import com.study.mobile.imagetospeech.util.CustomPlayer;
import com.study.mobile.imagetospeech.util.TesseractWrapper;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private static final int CAMERA_REQUEST_CODE = 100;
    private static final int OPEN_IMAGE_REQUEST_CODE = 101;

    private String textToSpeech = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onSettingsButtonClick(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    public void onExitButtonClick(View view) {
        finish();
    }

    public void onCameraButtonClick(View view) {
        Intent intent = new Intent(this, CameraActivity.class);

        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    public void onSpeakButtonClick(View view) {

        Log.e("MAIN", "Text to speech: " + textToSpeech);
        new CustomPlayer().execute(textToSpeech);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult executing");

        if (resultCode != RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (data.hasExtra("textToSpeech")) {
                    String text = data.getExtras().getString("textToSpeech");

                    if (!text.isEmpty()) {
                        textToSpeech = text;
                    }
                }
                break;
            case OPEN_IMAGE_REQUEST_CODE:
                Uri uri = data.getData();
                Log.d(TAG, "File Uri: " + uri.toString());

                TesseractWrapper tess = new TesseractWrapper(this);
                String text = tess.doOCR(uri);

                Intent intent = new Intent(this, CameraActivity.class);
                intent.putExtra("text", text);
                startActivityForResult(intent, CAMERA_REQUEST_CODE);

                break;
        }
    }

    public void onOpenFileButtonClick(View view) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    OPEN_IMAGE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
